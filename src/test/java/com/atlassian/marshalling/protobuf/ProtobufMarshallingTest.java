package com.atlassian.marshalling.protobuf;

import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import com.atlassian.marshalling.protobuf.gen.TestCaches.TestCacheData;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class ProtobufMarshallingTest {
    @Test(expected = MarshallingException.class)
    public void testIllegalData()  {
        final MarshallingPair<TestCacheData> mp =
                ProtobufMarshalling.pair(TestCacheData.PARSER);
        mp.getUnmarshaller().unmarshallFrom(new byte[] {1});
    }

    @Test
    public void testLegalData()  {
        final MarshallingPair<TestCacheData> mp =
                ProtobufMarshalling.pair(TestCacheData.PARSER);
        final TestCacheData indata = TestCacheData.newBuilder().setPageId(666L).build();
        final byte[] raw = mp.getMarshaller().marshallToBytes(indata);

        assertThat(raw, notNullValue());

        final TestCacheData outdata = mp.getUnmarshaller().unmarshallFrom(raw);

        assertThat(outdata, notNullValue());
        assertThat(outdata, is(indata));
    }
}
